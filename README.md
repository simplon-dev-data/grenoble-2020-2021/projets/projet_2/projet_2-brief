![image d'illustration](illustration.jpg)

Le 10 juillet 2020, le Conseil d'Etat a rendu publique une décision historique pour contraindre l'Etat Français à prendre des mesures immédiates en faveur de la qualité de l'air. Les seuils fixés par l'Europe pour les particules **PM10** et le dioxyde d'azote **NO2** doivent être respectés sous peine de lourdes amendes.

En Auvergne-Rhône-Alpes, différentes zones sont concernées par ces dépassements. La région a donc besoin de différents outils pour **suivre et analyser la pollution atmosphérique** à l'échelle de la région.

Suite à des réunions, trois besoins ont été retenus :

1 - La direction de la région en charge de la pollution de l'air souhaite avoir un **tableau de bord** avec différents **indicateurs globaux (échelle de la région)** concernant la qualité de l'air. Pour avoir une vision plus détaillée, elle souhaite aussi avoir une **cartographie de la région** avec les différents polluants atmosphériques **mise à jour toutes les heures**.

2 - Avec la même source de données que pour le besoin précédent, le **service de datascience** de la région souhaite avoir un **notebook Jupyter** pour pouvoir faire travailler ses équipes de datascience. Elle a besoin que le notebook contienne toutes **les explications** pour faire des requêtes sur la base de données ainsi qu'une cartographie. Le but est que les data scientist soient très vite opérationnels sur les données.

3 - Suite à la pandémie de COVID-19 qui a débuté au début de l'année 2020, la région souhaite connaître l'**impact du confinement** sur la pollution atmosphérique dans la région. 

Dans la région, différentes stations de mesures de la pollution de l'air sont implantées. Les données sont accessibles grâce à une API à l'adresse suivante : https://data-atmoaura.opendata.arcgis.com/

## Modalités d'évaluation

Pour que les livrables correspondent au mieux aux attentes de la région, deux points d'étape vous sont demandés pour présenter l'avancement du projet :

- **13 janvier 2021** : présentation courte (2 minutes) par groupe pour indiquer le planning et l'organisation du projet, une maquette des livrables et des schémas (base de données et workflow de la donnée)

- **20 janvier 2021** : présentation (5 minutes) par groupe d'une première version des livrables pour montrer l'avancement du projet

La présentation finale du projet aura lieu la semaine du 25 au 29 janvier suivant ces modalités :

- 30 minutes maximum de presentation
- 30 minutes maximum de débrief

## Technologies

Git, Bash, SQL, PostgreSQL, Python, Metabase

## Livrables

Copies d'écran des deux tableaux de bord (besoins 1 et 3) - Jupyter Notebook (besoin 2) - Dépôt GitLab - Code source fonctionnel - Documents de réflexion et de conception - L'outil de suivi d'avancement du projet

## Compétences visées

- C1. Concevoir et structurer physiquement une base de données relationnelle ou non, à partir des besoins, contraintes et données du commanditaire

**niveau 2, adapter**

- C2. Acquérir des données, les combiner et les structurer en données propres en vue de leur intégration dans la structure de la base de données

**niveau 3, transposer**

- C3. Intégrer des données propres et préparées dans la base de données finale, en utilisant des langages informatiques, logiciels ou outils

**niveau 3, transposer**

- C5. Interroger la base de données afin de mettre à jour les données (brutes ou traitées) stockées, provisoirement ou durablement, en fonction du résultat recherché

**niveau 2, adapter**

- C6. Concevoir et réaliser un rendu visuel des données issues du processus d'extraction, à l’aide d’un (des) support(s) adapté(s) répondant aux attentes du commanditaire

**niveau 3, transposer**

- C8. Analyser et formaliser la demande ou le besoin en développement et en exploitation de base de données

**niveau 2, adapter**

- C9. Autocontrôler, tout au long du processus de développement, la cohérence des données et la conformité à la demande

**niveau 2, adapter**

- C10. Suivre, adapter et rendre compte de la réalisation du projet à partir du planning projet validé

**niveau 2, adapter**

- C11. Rechercher des solutions pour la résolution de problèmes techniques rencontrés au moyen des ressources disponibles (documentation, sites Internet, communautés, etc..)

**niveau 2, adapter**
